// INIT
console.log("STARTING SCRIPT");

// IMPORTS
const fs = require("fs");
const images = require("images");
const IniFile = require('read-ini-file');
console.log("IMPORTS COMPLETED");

// EXTRA CONFIGS
const OUPUTFOLDER = "./output/";
const CONFIGS = IniFile.sync("./config.ini");
const isTucked = CONFIGS['isTucked'] ?? false;
console.log("CONFIGS LOADED");

// GET DATA
const BASEIMAGE = "./data/base.png";
const BACKGROUNDS = fs.readdirSync("./data/backgrounds/", {withFileTypes: true}).filter(item => !item.isDirectory()).map(item => "./data/backgrounds/" + item.name);
const TSHIRTS = fs.readdirSync("./data/tshirts/", {withFileTypes: true}).filter(item => !item.isDirectory()).map(item => "./data/tshirts/" + item.name);
const PANTS = fs.readdirSync("./data/pants/", {withFileTypes: true}).filter(item => !item.isDirectory()).map(item => "./data/pants/" + item.name);
console.log("DATA IMPORT COMPLETED");

// GENERATE FILE COMBINATIONS
let DESIGNS = [];
BACKGROUNDS.forEach(background => {
    TSHIRTS.forEach(tshirt => {
        PANTS.forEach(pants => {
            DESIGNS.push({
                base: BASEIMAGE,
                background: background,
                tshirt: tshirt,
                pants: pants,
            });         
        });        
    });    
});
console.log("FILE COMBINATION GENERATION COMPLETED");

// GENERATE OUTPUT FILES
DESIGNS.forEach(design => {
    let RANDOM_STRING = Math.random().toString(36).slice(2, 12);
    if(isTucked){
        images(design['background'])
        .draw(images(design['base']), 0, 0)
        .draw(images(design['tshirt']), 0, 0)
        .draw(images(design['pants']), 0, 0)
        .save(OUPUTFOLDER + RANDOM_STRING + ".jpg", {quality : 90});
    } else {
        images(design['background'])
        .draw(images(design['base']), 0, 0)
        .draw(images(design['pants']), 0, 0)
        .draw(images(design['tshirt']), 0, 0)
        .save(OUPUTFOLDER + RANDOM_STRING + ".jpg", {quality : 90});
    }
});

console.log("OUTPUT GENERATION COMPLETED");
